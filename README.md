Build go binary from the root of repository

### CLI arguments:

- path to folder of the processed first video JSON files (the folder contains JSON-files, each file contains OpenPose structured 25 Points coordinates)
- path to folder of the processed second video JSON files (the folder contains JSON-files, each file contains OpenPose structured 25 Points coordinates)
- 3 - ... Joint triads - 3 Joints numbers for an Angle to be processed ([see Joints numbers](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#pose-output-format-body_25))

#### CLI arguments example:

`~/squatting/good_1/json ~/squatting/bad_5/json 20 14 13 14 13 12 13 12 1`

In this example app will parse JSON files with Poses from the`~/squatting/good_1/json` and `~/squatting/bad_5/json` folders.
3 Angles will be counted by the app: `20 14 13`, `14 13 12` and `13 12 1`.
