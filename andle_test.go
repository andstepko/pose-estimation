package main

import (
	"testing"
	"fmt"
)

func TestCountAngle(t *testing.T) {
	p1 := Point{
		X: 1,
		Y: -4,
	}
	p2 := Point{
		X: 0,
		Y: 0,
		Confidence: 1,
	}
	p3 := Point{
		X: 3,
		Y: 1,
	}

	a := NewAngle(p1, p2, p3)
	fmt.Println(a.InDegrees())
}

func TestAngleDelta(t *testing.T) {
	a1 := Angle(0.78)
	a2 := Angle(-0.78)

	fmt.Println(a1.Delta(a2).InDegrees())
}
