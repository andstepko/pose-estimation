package main

import (
	"math"
)

type Angle float64

func (a Angle) Delta(a2 Angle) Angle {
	if (a > 0) != (a2 > 0) {
		// Different signs
		result := math.Abs(a.Float64()) + math.Abs(a2.Float64())
		if result > math.Pi {
			result = 2*math.Pi - result
		}
		return Angle(result)
	} else {
		return Angle(math.Abs(float64(a2 - a)))
	}
}

func (a Angle) Float64() float64 {
	return float64(a)
}
func (a Angle) AbsFloat64() float64 {
	return math.Abs(float64(a))
}

func (a Angle) InDegrees() float64 {
	return (float64(a) / math.Pi) * float64(180)
}

func NewAngle(p1, p2, p3 Point) *Angle {
	if p1.IsZero() || p2.IsZero() || p3.IsZero() {
		return nil
	}

	p21 := Point{
		X: p1.X - p2.X,
		Y: p1.Y - p2.Y,
	}
	p23 := Point{
		X: p3.X - p2.X,
		Y: p3.Y - p2.Y,
	}

	b := p1.DistanceTo(p2)
	c := p2.DistanceTo(p3)

	cos := (p21.X*p23.X + p21.Y*p23.Y) / (b*c)
	angle := math.Acos(cos)

	zVectorMult := p21.X*p23.Y - p21.Y*p23.X
	if zVectorMult < 0 {
		angle = -angle
	}

	res := Angle(angle)
	return &res
}

func InDegrees(angles []Angle) []float64 {
	var result []float64

	for _, a := range angles {
		result = append(result, a.InDegrees())
	}

	return result
}

func MinMax(angles []Angle) (minAngle, maxAngle Angle) {
	if len(angles) == 0 {
		return 0, 0
	}

	min := angles[0].AbsFloat64()
	max := angles[0].AbsFloat64()

	for _, a := range angles {
		if a.AbsFloat64() < min {
			min = a.AbsFloat64()
		}
		if a.AbsFloat64() > max {
			max = a.AbsFloat64()
		}
	}

	return Angle(min), Angle(max)
}
