package main

func DWT(dMatrix [][]float64) float64 {
	n := len(dMatrix)
	m := len(dMatrix[0])

	// Building matrix of transformations

	// First row
	for j := 1; j < m; j++ {
		dMatrix[0][j] = dMatrix[0][j] + dMatrix[0][j-1]
	}
	// First column
	for i := 1; i < n; i++ {
		dMatrix[i][0] = dMatrix[i][0] + dMatrix[i-1][0]
	}

	for i := 1; i < n; i++ {
		for j := 1; j < m; j++ {
			minNeighbour, _ := minOf3(dMatrix[i-1][j], dMatrix[i-1][j-1], dMatrix[i][j-1])
			dMatrix[i][j] = dMatrix[i][j] + minNeighbour
		}
	}

	// Count length of the route
	i := n - 1
	j := m - 1
	count := 1
	for i > 0 && j > 0 {
		_, which := minOf3(dMatrix[i-1][j], dMatrix[i-1][j-1], dMatrix[i][j-1])
		switch which {
		case 0:
			i -= 1
		case 1:
			i -= 1
			j -= 1
		case 2:
			j -= 1
		}

		count += 1
	}

	count += i + j

	return dMatrix[n-1][m-1] / float64(count)
}

func minOf3(x1, x2, x3 float64) (float64, int) {
	if x1 <= x2 && x1 <= x3 {
		return x1, 0
	}
	if x2 <= x1 && x2 <= x3 {
		return x2, 1
	}

	return x3, 2
}

// DTW comparison didn't work for Angles, this func is unused now, but remains here just in case
func compareAngleRowsDTW(q, c []Angle) float64 {
	var n = len(q)
	var m = len(c)

	if n == 0 || m == 0 {
		panic("empty slice(s) of Angles cannot be compared")
	}

	dMatrix := initMatrix(n, m)

	for i := 0; i < n; i++ {
		for j := 0; j < m; j++ {
			dMatrix[i][j] = float64(q[i].Delta(c[j]))
		}
	}

	return DWT(dMatrix)
}

func initMatrix(n, m int) [][]float64 {
	result := make([][]float64, n)

	for i := 0; i < n; i++ {
		result[i] = make([]float64, m)
	}

	return result
}
