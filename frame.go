package main

type Frame struct {
	//Version float64
	People []Pose `json:"people"`
}

// LessZeroPerson panics if People slice is empty
func (f Frame) LeastZeroPerson() (Pose, int) {
	if len(f.People) == 0 {
		panic("must be non-empty slice of People")
	}

	zeros := f.People[0].ZeroPoints()
	result := f.People[0]
	for i := 1; i < len(f.People); i++ {
		t := f.People[i].ZeroPoints()

		if t < zeros {
			zeros = t
			result = f.People[i]
		}
	}

	return result, zeros
}
