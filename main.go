package main

import (
	"fmt"
	"math"
	"os"
)

const (
	keypointsNumber = 25
)

func main() {
	args := os.Args[1:]

	if len(args) < 2 || (len(args) - 2) % 3 != 0 {
		fmt.Println("wrong number of arguments")
		os.Exit(1)
		return
	}

	jointTriads := NewTriads(args[2:])
	if len(jointTriads) == 0 {
		fmt.Println("no JointTriads were provided in the cli argments")
		os.Exit(1)
		return
	}

	folder1Path := args[0]
	folder2Path := args[1]


	anglesRows1 := parseAngles(folder1Path, jointTriads)
	anglesRows2 := parseAngles(folder2Path, jointTriads)

	differences := make([]float64, len(jointTriads))
	for i := 0; i < len(jointTriads); i++ {
		fmt.Println("Angle ", jointTriads[i])

		differences[i] = compareAngleRowsExtremums(anglesRows1[i], anglesRows2[i])

		fmt.Println()
	}

	fmt.Println("Angles differences (in parts from Pi):", differences)
}

// CompareAngleRowsExtremums returns part from math.Pi
func compareAngleRowsExtremums(q, c []Angle) float64 {
	qMin, qMax := MinMax(q)
	cMin, cMax := MinMax(c)

	fmt.Println("First extremums:", qMin.InDegrees(), qMax.InDegrees())
	fmt.Println("Second extremums:", cMin.InDegrees(), cMax.InDegrees())

	totalDeltaRadians := math.Abs(qMin.Float64() - cMin.Float64()) + math.Abs(qMax.Float64() - cMax.Float64())
	fmt.Println("Total delta in degrees:", Angle(totalDeltaRadians).InDegrees())
	return totalDeltaRadians / math.Pi
}

