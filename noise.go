package main

import "fmt"

func removeNoise(poses []Pose, jointsToConsider []int) []Pose {
	if len(poses) == 0 || len(jointsToConsider) == 0 {
		return poses
	}

	const noiseThreshold = 0.075

	for _, pointNumber := range jointsToConsider {
		// For each Joint
		newPoses := []Pose{poses[0]}

		basePoint := poses[0].Points[pointNumber]
		for poseI := 0; poseI < len(poses)-1; poseI++ {
			if basePoint.IsZero() {
				// First valid Point hasn't been found yet - search for valid basePoint
				curPoint := poses[poseI].Points[pointNumber]

				if curPoint.IsZero() {
					newPoses = append(newPoses, poses[poseI])
					continue
				}

				// Found the first valid Point - using it as the first basePoint
				basePoint = curPoint
			}

			point2 := poses[poseI+1].Points[pointNumber]

			if point2.IsZero() {
				// Not refreshing the basePoint, but it's not a noise - not removing this Pose
				newPoses = append(newPoses, poses[poseI+1])
				continue
			}

			// point2 is not zero
			shift := basePoint.DistanceTo(point2)
			if shift <= noiseThreshold {
				// Not a noise: Refreshing the basePoint, not removing this Pose
				basePoint = point2
				newPoses = append(newPoses, poses[poseI+1])
			}
		}

		if 2*len(newPoses) > len(poses) {
			// It's OK to filter, not many Poses removed
			//fmt.Println(fmt.Sprintf("Poses filtering for the Joint#%d: was %d, got %d, removed %d", pointNumber, len(poses), len(newPoses), len(poses)-len(newPoses)))
			poses = newPoses
		} else {
			fmt.Println(fmt.Sprintf("Rejected Poses filtering for the Joint#%d: was %d, got %d, removed %d. Cannot remove so many Poses.",
				pointNumber, len(poses), len(newPoses), len(poses)-len(newPoses)))
		}
	}

	//fmt.Println()

	return poses
}
