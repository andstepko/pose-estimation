package main

import (
	"os"
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"sort"
	"fmt"
)

func parseAngles(folderPath string, jointTriads []JointTriad) [][]Angle {
	poses, err := parsePoses(folderPath)
	if err != nil {
		panic(errors.Wrap(err, "failed to parse poses from the directory"))
	}

	var flags [keypointsNumber]bool
	for _, triad := range jointTriads {
		flags[triad[0]] = true
		flags[triad[1]] = true
		flags[triad[2]] = true
	}

	var joints []int
	for i := range flags {
		if flags[i] {
			joints = append(joints, i)
		}
	}

	poses = removeNoise(poses, joints)
	//poses = removeNoise(poses, []int{11}) // RAnkle
	//poses = removeNoise(poses, []int{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24})

	return toAnglesRows(poses, jointTriads)
}

func parsePoses(folderPath string) ([]Pose, error) {
	dir, err := os.OpenFile(folderPath, os.O_RDONLY, os.ModeDir)
	if err != nil {
		return nil, errors.Wrap(err, "failed to open the directory")
	}

	fileNames, err := dir.Readdirnames(0)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read list of files in the directory")
	}

	sort.Strings(fileNames)

	var poses []Pose
	for _, f := range fileNames {
		fPath := folderPath + "/" + f
		frame, err := parseFrame(fPath)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("failed to parse Frame (%s)", fPath))
		}

		if len(frame.People) == 0 {
			// TODO Make prettier
			fmt.Println("skipping frame", fPath)
			continue
		}

		pose, _ := frame.LeastZeroPerson()
		poses = append(poses, pose)
	}

	return poses, nil
}

func parseFrame(filePath string) (*Frame, error) {
	jsonFile, err := os.OpenFile(filePath, os.O_RDONLY, 0444)
	if err != nil {
		return nil, errors.Wrap(err, "failed to open the file")
	}

	var pf Frame
	err = json.NewDecoder(jsonFile).Decode(&pf)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode JSON form the file content")
	}

	return &pf, nil
}

func toPointsRows(poses []Pose) [keypointsNumber][]Point {
	var result = [keypointsNumber][]Point{}

	for _, pose := range poses {
		for pointI, point := range pose.Points {
			result[pointI] = append(result[pointI], point)
		}
	}

	return result
}

func toAnglesRows(poses []Pose, angles []JointTriad) [][]Angle {
	result := make([][]Angle, len(angles))

	for _, pose := range poses {
		for angleI, angle := range pose.GetAngles(angles) {
			if angle != nil {
				result[angleI] = append(result[angleI], *angle)
			}
		}
	}

	return result
}
