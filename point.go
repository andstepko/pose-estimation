package main

import "math"

type Point struct {
	X          float64
	Y          float64
	Confidence float64
}

func (p Point) IsZero() bool {
	// Y is compared with 1 because it 0 Y-s are transformed into 1.
	return p.X == 0 && p.Y == 1 && p.Confidence == 0
}

func (p Point) DistanceTo(p2 Point) float64 {
	dX := p2.X - p.X
	dY := p2.Y - p.Y

	return math.Sqrt(dX*dX + dY*dY)
}
