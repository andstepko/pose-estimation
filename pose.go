package main

import (
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

//     {0,  "Nose"},
//     {1,  "Neck"},
//     {2,  "RShoulder"},
//     {3,  "RElbow"},
//     {4,  "RWrist"},
//     {5,  "LShoulder"},
//     {6,  "LElbow"},
//     {7,  "LWrist"},
//     {8,  "MidHip"},
//     {9,  "RHip"},
//     {10, "RKnee"},
//     {11, "RAnkle"},
//     {12, "LHip"},
//     {13, "LKnee"},
//     {14, "LAnkle"},
//     {15, "REye"},
//     {16, "LEye"},
//     {17, "REar"},
//     {18, "LEar"},
//     {19, "LBigToe"},
//     {20, "LSmallToe"},
//     {21, "LHeel"},
//     {22, "RBigToe"},
//     {23, "RSmallToe"},
//     {24, "RHeel"},
//     {25, "Background"}

type Pose struct {
	Points [keypointsNumber]Point
}

func (p Pose) ZeroPoints() int {
	var count int

	for _, point := range p.Points {
		if point.IsZero() {
			count += 1
		}
	}

	return count
}

func (p *Pose) UnmarshalJSON(data []byte) error {
	t := struct {
		PoseKeypoints2D []float64 `json:"pose_keypoints_2d"`
	} {}

	err := json.Unmarshal(data, &t)
	if err != nil {
		return errors.Wrap(err, "failed to unmarshal Pose object")
	}

	if len(t.PoseKeypoints2D) % 3 != 0 {
		return errors.Errorf("number of pose_keypoints_2d (%d) is not multiple of 3", len(t.PoseKeypoints2D))
	}
	if len(t.PoseKeypoints2D)  != keypointsNumber * 3 {
		return errors.Errorf("number of pose_keypoints_2d expected to be 75, got (%d)", len(t.PoseKeypoints2D))
	}

	var points [keypointsNumber]Point
	for i := 0; i < len(t.PoseKeypoints2D) / 3; i++ {
		x := t.PoseKeypoints2D[i*3]
		// To convert layout coordinates to classic Dekart
		y := 1 - t.PoseKeypoints2D[i*3 +1]

		if (x > 1) || (x < 0) || (y > 1) || (y < 0) {
			return errors.Errorf("Point number (%d) has coordinates not in [0;1]", i)
		}

		point := Point{
			X: x,
			Y: y,
			Confidence: t.PoseKeypoints2D[i*3 +2],
		}

		points[i] = point
	}

	p.Points = points
	return nil
}

func (p Pose) GetAngles(angles []JointTriad) []*Angle {
	var result []*Angle
	for _, triad := range angles {
		a := NewAngle(p.Points[triad[0]], p.Points[triad[1]], p.Points[triad[2]])
		result = append(result, a)
	}

	return result
}
