package main

import "math"

func FloatMinMaxAbs(row []float64) (min, max float64) {
	if len(row) == 0 {
		return 0, 0
	}

	min = row[0]
	max = row[0]

	for _, elem := range row {
		eAbs := math.Abs(toFloat(elem))

		if eAbs < min {
			min = eAbs
		}
		if eAbs > max {
			max = eAbs
		}
	}

	return min, max
}

func FloatMaxAbs(row []float64) float64 {
	if len(row) == 0 {
		return 0
	}

	max := row[0]

	for _, elem := range row {
		eAbs := math.Abs(toFloat(elem))

		if eAbs > max {
			max = eAbs
		}
	}

	return max
}

func RowAverage(row []float64) float64 {
	var sum float64

	for _, f := range row {
		if f < 0 {
			panic("average can be found among non-negative numbers")
		}

		sum += f
	}

	return sum / float64(len(row))
}

func MinMaxAbs(row []interface{}) (min, max float64) {
	if len(row) == 0 {
		return 0, 0
	}

	min = toFloat(row[0])
	max = toFloat(row[0])

	for _, elem := range row {
		eAbs := math.Abs(toFloat(elem))

		if eAbs < min {
			min = eAbs
		}
		if eAbs > max {
			max = eAbs
		}
	}

	return min, max
}


type Float interface {
	Float() float64
}

func toFloat(i interface{}) float64 {
	switch i.(type) {
	case Float:
		return i.(Float).Float()
	case float64:
		return i.(float64)
	default:
		return i.(float64)
	}
}
