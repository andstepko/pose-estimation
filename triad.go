package main

import "strconv"

type JointTriad [3]int

func NewTriads(args []string) []JointTriad {
	var result []JointTriad

	for i := 0; i < len(args)/3; i++ {
		first, err := strconv.Atoi(args[3*i])
		if err != nil {
			panic(err)
		}
		second, err := strconv.Atoi(args[3*i+1])
		if err != nil {
			panic(err)
		}
		third, err := strconv.Atoi(args[3*i+2])
		if err != nil {
			panic(err)
		}

		result = append(result, JointTriad{first, second, third})
	}

	return result
}
